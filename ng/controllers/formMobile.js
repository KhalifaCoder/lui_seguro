'use strict';
angular.module('luiSeguroApp')
  .controller('formMobileCtrl', function ($scope) {
    $('html, body').animate({scrollTop:0}, 'slow');
    $scope.changeHeader=function(){
      document.getElementById('backheadermobile').classList.remove("backgroundmobile");
      document.getElementById('backheadermobile2').classList.remove("backheadermobile");
      document.getElementById('backheadermobile2').classList.add("backheader2");
      document.getElementById('body').classList.remove("backgroundblue");
      document.getElementById('body').classList.add("backgroundgray");
    };
    $scope.changeHeader();
  });
