'use strict';
angular.module('luiSeguroApp')
    .controller('formCotizacionCtrl', function($scope,$rootScope) {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $rootScope.interna=1;
        function cambioMediaQuery(mql) {
            if (mql.matches) {
                document.getElementById('backheadermobile').classList.remove("backgroundmobile");
                document.getElementById('backheadermobile2').classList.remove("backheadermobile");
                 document.getElementById('body').classList.add("backgroundblue");
                document.getElementById('backheadermobile2').classList.add("backheader2");
            }
            else {
                document.getElementById('backheader').classList.remove("backheader2");
                document.getElementById('backheader').classList.add("backheader");
                document.getElementById('body').classList.add("backgroundblue");
                document.getElementById('body').classList.remove("backgroundgray");
            }
        }

        var mql = window.matchMedia("screen and (max-width: 991px)");
        cambioMediaQuery(mql);
        mql.addListener(cambioMediaQuery);
    });
