'use strict';
angular.module('luiSeguroApp')
  .controller('MainCtrl', function($scope, $rootScope) {
    $rootScope.TiposDocumentos = [
      { id: "1", nombre: "Cédula de Ciudadanía", tipomovil: "1", tipofijo: "CC" },
      { id: "2", nombre: "Cédula de Extranjería", tipomovil: "4", tipofijo: "CE" },
      { id: "3", nombre: "Pasaporte", tipomovil: "3", tipofijo: "PP" },
      { id: "4", nombre: "Carné Diplomático", tipomovil: "-1", tipofijo: "CD" },
      { id: "5", nombre: "Nit", tipomovil: "2", tipofijo: "NI" }
    ];
    $('html, body').animate({ scrollTop: 0 }, 0);
    $rootScope.interna=0;
    $scope.infoseguro = true;

    function cambioMediaQuery(mql) {
      if (mql.matches) {
        document.getElementById('backheadermobile').classList.add("backgroundmobile");
        document.getElementById('backheadermobile2').classList.add("backheadermobile");
        document.getElementById('backheadermobile2').classList.remove("backheader2");
      }
      else {
        document.getElementById('backheader').classList.remove("backheader2");
        document.getElementById('backheader').classList.add("backheader");
        document.getElementById('body').classList.remove("backgroundblue");
        document.getElementById('body').classList.remove("backgroundgray");
      }
    }

    var mql = window.matchMedia("screen and (max-width: 991px)");
    cambioMediaQuery(mql);
    mql.addListener(cambioMediaQuery);


    $scope.infoSarlaft = function() {
      $scope.sarlaft = true;
      $scope.infoseguro = false;
      $scope.infoasegurar = false;
      $scope.infocompanias = false;
    };
    $scope.infoCompanias = function() {
      $scope.sarlaft = false;
      $scope.infoseguro = false;
      $scope.infoasegurar = false;
      $scope.infocompanias = true;
    };
    $scope.infoAsegurar = function() {
      $scope.sarlaft = false;
      $scope.infoseguro = false;
      $scope.infoasegurar = true;
      $scope.infocompanias = false;
    };
    $scope.infoSeguro = function() {
      $scope.sarlaft = false;
      $scope.infoseguro = true;
      $scope.infoasegurar = false;
      $scope.infocompanias = false;
    };
    var acc = document.getElementsByClassName("accordion");
    var panel = document.getElementsByClassName('panel');

    for (var i = 0; i < acc.length; i++) {
      acc[i].onclick = function() {
        var setClasses = !this.classList.contains('active');
        setClass(acc, 'active', 'remove');
        setClass(panel, 'show', 'remove');

        if (setClasses) {
          this.classList.toggle("active");
          this.nextElementSibling.classList.toggle("show");
        }
      }
    }

    function setClass(els, className, fnName) {
      for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
      }
    }
  });
