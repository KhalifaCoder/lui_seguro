'use strict';
angular.module('luiSeguroApp')
  .controller('confirmacionCtrl', function ($scope,$rootScope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $('html, body').animate({scrollTop:0}, 'slow');
    $rootScope.interna=1;
     function cambioMediaQuery(mql) {
      if (mql.matches) {
        document.getElementById('backheadermobile').classList.remove("backgroundmobile");
        document.getElementById('backheadermobile2').classList.remove("backheadermobile");
        document.getElementById('backheadermobile2').classList.add("backheader2");
        document.getElementById('body').classList.add("backgroundgray");
      }
      else {
         document.getElementById('backheader').classList.remove("backheader");
      document.getElementById('backheader').classList.add("backheader2");
      document.getElementById('body').classList.remove("backgroundblue");
      document.getElementById('body').classList.add("backgroundgray");
      }
    }

    var mql = window.matchMedia("screen and (max-width: 991px)");
    cambioMediaQuery(mql);
    mql.addListener(cambioMediaQuery);
  });
