'use strict';
angular
  .module('luiSeguroApp', [
    'ngRoute','ui.bootstrap'
  ]).run(function($rootScope,$location){
    $rootScope.fcninterna=function(ruta){
      window.location.href= "#!/"+ruta;
    };
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/cotizacion', {
        templateUrl: 'views/cotizacion.html',
        controller: 'cotizacionCtrl',
        controllerAs: 'cotizacion'
      })
      .when('/formCotizacion', {
        templateUrl: 'views/formCotizacion.html',
        controller: 'formCotizacionCtrl',
        controllerAs: 'formCotizacion'
      })
      .when('/confirmacion', {
        templateUrl: 'views/confirmacion.html',
        controller: 'confirmacionCtrl',
        controllerAs: 'confirmacion'
      }).when('/formMobile', {
        templateUrl: 'views/formMobile.html',
        controller: 'formMobileCtrl',
        controllerAs: 'formMobile'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
